import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    appId: undefined,
    appSecret: undefined,
    authToken: undefined,

    menu: undefined
  },

  getters: {
    hasAppData: state => {
      return state.appId !== undefined && state.appSecret !== undefined && state.authToken !== undefined
    },

    appHeaders: state => {
      return {
        'app-id': state.appId,
        'app-secret': state.appSecret
      }
    },

    appAuthHeaders: state => {
      return {
        'app-id': state.appId,
        'app-secret': state.appSecret,
        'auth-token': state.authToken
      }
    },

    menuCategories: state => {
      return state.menu
    }
  },

  mutations: {
    setAppData: (state, payload) => {
      state.appId = payload.appId
      state.appSecret = payload.appSecret
      state.authToken = payload.authToken
    },

    clearAppData: state => {
      state.authToken = undefined
    },

    setMenu: (state, payload) => {
      state.menu = payload
    }
  }
})
