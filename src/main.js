
import Vue from 'vue'
import App from './App.vue'

import Axios from 'axios'
import {store} from './store/store'
import {router} from './router'

Vue.prototype.$http = Axios

new Vue({ // eslint-disable-line no-new
  el: '#app',
  store,
  router,
  render: (h) => h(App)
})
