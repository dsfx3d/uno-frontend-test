import Vue from 'vue'
import VueRouter from 'vue-router'

import LoginPage from '../pages/LoginPage.vue'
import HomePage from '../pages/HomePage.vue'

import MenuWidget from '../components/MenuWidget.vue'
import MenuWidgetPanes from '../components/MenuWidgetCategoryPanes.vue'

Vue.use(VueRouter)

const routes = [
  {path: '/', redirect: '/login'},
  {path: '/login', name: 'login', component: LoginPage},
  //
  {
    path: '/restaurants',
    name: 'home',
    component: HomePage,
    children: [
      {
        path: ':restaurant_id/menu',
        name: 'restaurant-menu',
        component: MenuWidget,
        children: [
          {
            path: 'category/:category_id',
            name: 'category-menu',
            component: MenuWidgetPanes
          }
        ]
      }
    ]
  }
]

export const router = new VueRouter({
  routes
})
